/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_list_push_back.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: arherrer <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/07 17:52:13 by arherrer          #+#    #+#             */
/*   Updated: 2018/08/07 18:03:01 by arherrer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "ft_list.h"

void		ft_list_push_back(t_list **begin_list, void *data)
{
	t_list	*list;
	t_list	*tmp;

	list = ft_create_elem(data);
	tmp = *begin_list;
	while (tmp -> next -> next)
	{
		if (tmp -> next == NULL)
		{
			tmp -> next = list;
			return ;
		}
		tmp = tmp -> next;
	}
}
