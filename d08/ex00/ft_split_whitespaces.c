/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_split_whitespaces.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: arherrer <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/01 21:29:05 by arherrer          #+#    #+#             */
/*   Updated: 2018/08/02 00:51:33 by arherrer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <stdlib.h>

size_t	c_isany(const char c, const char *str, const size_t n)
{
	if (n == 0)
		return (0);
	if (c == str[n - 1])
		return (1);
	else
		return (c_isany(c, str, n - 1));
}

size_t	c_isspace(const char c)
{
	const char		str[6] = {
		' ', '\t', '\r', '\n', '\v', '\f',
	};

	return (c_isany(c, str, 6));
}

size_t	get_num_of_words(const char *str)
{
	size_t	words;
	size_t	notallspace;
	char	cur;
	char	next;
	size_t	it;

	words = 0;
	cur = '\0';
	next = '\0';
	notallspace = 0;
	it = 0;
	while (str[it] != '\0' && c_isspace(str[it]))
		it = it + 1;
	while (str[it] != '\0')
	{
		cur = str[it];
		next = str[it + 1];
		if (!c_isspace(cur) && (c_isspace(next) || next == '\0'))
		{
			words = words + 1;
			notallspace = 1;
		}
		it = it + 1;
	}
	return (words + notallspace);
}

size_t	s_slen(const char *str, const size_t it)
{
	if (str[it] != '\0' && !c_isspace(str[it]))
	{
		return (1 + s_slen(str, it + 1));
	}
	return (0);
}

char	**ft_split_whitespaces(const char *str)
{
	const size_t	num_of_words = get_num_of_words(str);
	char			**words;
	size_t			i;
	size_t			j;
	size_t			k;

	if (!str || !(words = malloc(sizeof(char *) * (num_of_words + 1))))
		return (NULL);
	i = 0;
	j = 0;
	while (i < num_of_words)
	{
		if (!(words[i] = malloc(sizeof(char) * (s_slen(str, j) + 1))))
			return (NULL);
		while (c_isspace(str[j]))
			j = j + 1;
		k = 0;
		while (str[j] != '\0' && !c_isspace(str[j]))
		{
			words[i][k++] = str[j++];
		}
		words[i++][k] = '\0';
	}
	words[i] = NULL;
	return (words);
}
